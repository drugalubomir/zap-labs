#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define START 10
#define END 20
#define ATTEMPS 5

int get_guess(const int attemp);  // function declaration
int get_secret(const int start, const int end);  // function declaration

void clear_buffer() {
    while (getchar() != '\n');
}

int main() {
    srand(time(NULL));

    char play_again = 'y';

    while (play_again != 'n') {
        int secret = get_secret(START, END);
        int guess;
        int attemps = 0;

        printf("Pick a number between 1 and 100.\n");
        printf("SECRET: %d\n", secret);

        do {
            attemps++;
            guess = get_guess(attemps);  // stores returned value in variable
            if (guess > secret) {
                printf("Hmm... My number is smaller than yours.\n");
            } else if (guess < secret) {
                printf("Hmm... My number is bigger than yours.\n");
            } else {
                printf("Congratulations! You found it!\n");
            }
        } while (guess != secret & attemps < ATTEMPS);

        if (guess != secret) {
            printf("Game over. My number was %d.", secret);
        }

        clear_buffer();

        printf("Play again? (y/n):");
        scanf("%c", &play_again);
        clear_buffer();
    }
    printf("See you later!\n");


    return 0;
}

// function definition
int get_guess(const int attemp) {
    printf("Your %d. guess: ", attemp);

    int guess;
    scanf("%d", &guess);

    // returns value
    return guess;
}

int get_secret(const int start, const int end) {
    return (rand() % (end - start + 1)) + start;
}
