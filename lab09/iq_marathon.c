#include <stdio.h>

void draw(const int height, const int width, char world[][width + 1]);

char change_direction(const int x, const int y, const int finish_x, const int finish_y, const char direction);

char find_cow(const int height, const int width, int *x, int *y, char world[][width + 1]);

void find_finish(const int height, const int width, int *finish_x, int *finish_y, char world[][width + 1]);

int solve_maze(const int height, const int width, char world[][width + 1]);

int main() {

    FILE *fp = NULL;
    fp = fopen("maze1", "r");

    if (fp == NULL) {
        perror("Error opening file");
        return 1;
    }

    int width, height;
    fscanf(fp, "%d %d ", &width, &height); // za druhym %d je medzera, aby sme rovno mohli nacitavat do pola uz bez medzery

    char world[height][width + 1]; // +1 kvoli null terminatoru (\0)

    for (int idx_h = 0; idx_h < height; ++idx_h) {
        fgets(world[idx_h], width + 1, fp); //whole line

        fgetc(fp); // char '|' len aby sme ho preskocili, nepotrebujeme ukladat
    }
    fclose(fp);


    draw(height, width, world);
    return 0;
}

void draw(const int height, const int width, char world[][width + 1]) {
    for (int idx_h = 0; idx_h < height; ++idx_h) {
        printf("%s\n", world[idx_h]);
    }

    //alternativne nacitanie po znaku
//    for (int i = 0; i < height; ++i) {
//        for (int j = 0; j < width; ++j) {
//            printf("%c", world[i][j]);
//        }
//        printf("\n");
//    }
}

char find_cow(const int height, const int width, int *x, int *y, char world[][width + 1]) {

    for (int idx_h = 0; idx_h < height; ++idx_h) {
        for (int idx_w = 0; idx_w < width; ++idx_w) {
            char tmp = world[idx_h][idx_w];

            if (tmp == 'W' || tmp == 'S' || tmp == 'E' || tmp == 'N') {
                *x = idx_w; // upravujeme hodnotu na danej adrese, preto *x
                *y = idx_h;
                return tmp;
            }
        }
    }

    return 'N';
}

void find_finish(const int height, const int width, int *finish_x, int *finish_y, char world[][width + 1]) {

    for (int idx_h = 0; idx_h < height; ++idx_h) {
        for (int idx_w = 0; idx_w < width; ++idx_w) {
            char tmp = world[idx_h][idx_w];

            if (tmp == 'F') {
                *finish_x = idx_w;
                *finish_y = idx_h;
                return;
            }
        }
    }

}

char change_direction(const int x, const int y, const int finish_x, const int finish_y, const char direction) {
    if (direction == 'W' || direction == 'E') {
        return y < finish_y ? 'S' : 'N';
    }

    return x < finish_x ? 'E' : 'W';
}