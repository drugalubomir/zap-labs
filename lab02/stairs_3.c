#include <superkarel.h>

void climb_stair(); // function declaration - without executable code
void turn_right();


int main() {
    turn_on("stairs2.kw");

    step();


    while (!front_is_clear()) {
        climb_stair();
    }

//    moznost zapisat aj takto
//    while (front_is_clear() == false) {
//        climb_stair();
//    }


    while (beepers_in_bag()) {
        put_beeper();
    }

    turn_off();

    return 0;
}

// function implementation - with executable code
void climb_stair() {
    turn_left();
    step();
    turn_right();
    step();

    if (beepers_present()) {
        pick_beeper();
    }
}

void turn_right() {
    turn_left();
    turn_left();
    turn_left();
}
