#include <superkarel.h>

void climb_stair(); // function declaration - without executable code
void turn_right();


int main() {
    turn_on("stairs.kw");

    step();
    climb_stair();
    pick_beeper();
    climb_stair();
    pick_beeper();
    climb_stair();
    pick_beeper();
    climb_stair();
    put_beeper();
    put_beeper();
    put_beeper();

    turn_off();

    return 0;
}

// function implementation - with executable code
void climb_stair() {
    turn_left();
    step();
    turn_right();
    step();
}

void turn_right() {
    turn_left();
    turn_left();
    turn_left();
}
