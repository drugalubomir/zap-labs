# ZAP Week 2

## Spustenie

```shell script
make stairs_1
./stairs_1
```


## Úloha 2.3

- riešenie v [stairs_1.c](stairs_1.c)

úlohu sme svojim spôsobom vyriešili, avšak iba manuálnym zadávaním príkazov, ktoré sa nám opakujú príliš často
 
 
- riešenie v [stairs_2.c](stairs_2.c)
    
úlohu sme vyriešili, vykoná sa presne rovnako ako v riešení [stairs_1.c](stairs_1.c)

pomocou funkcií `climb_stair()` a `turn_right()` si vieme opakujúci sa kód extrahovať do funkcie a danú funkciu 
potom volať opakovane namiesto viacerých príkazov, ktoré sú v danej funkcií

deklarácia funkcie - ak sa funkcia nachádza pod main-om (implementácia funkcie), 
je ju potrebné zadeklarovať aby program o danej funkcií vedel - ináč ju nerozpozná

deklarovať funkciu nemusíme, ak jej implementáciu napíšeme nad `main()`

výsledkom je teda **kratšie a prehľadnejšie** riešenie.
 
 
## Úloha 2.5

- ak použijeme kód (s upraveným názvom mapy) z riešenia v [stairs_2.c](stairs_2.c), 
vidíme, že program sa neukončí úspešne, keďže naše riešenie očakáva, že na druhom schode bude beeper

- riešenie v [stairs_3.c](stairs_3.c)

`pick_beeper()` funkcia bola presunutá do funkcie `climb_stair()`

funkcia bola presunutá do konštrukcie `if` - vykoná príkaz/y jedenkrát vo svojom tele, ak platí podmienka v okrúhlych zátvorkách 
```c
if (beepers_present()) {
    pick_beeper();
}
```
---

po vyšplhaní na najvyšší schod sme taktiež upravili aj vykladanie beeprov nasledovne

```c
while (beepers_in_bag()) {
    put_beeper();
}
```
cyklus `while` vykonáva príkaz/y vo svojom tele, pokiaľ platí podmienka v okrúhlych zátvorkách

---

následne funkcia main vyzerá takto:

```c
int main() {
    turn_on("stairs2.kw");

    step();

    climb_stair();
    climb_stair();
    climb_stair();
    climb_stair();
    climb_stair();

    while (beepers_in_bag()) {
        put_beeper();
    }

    turn_off();

    return 0;
}
```

ako vidíme, `climb_stair();` sa nám tu často opakuje, vieme ho nahradiť taktiež cyklom 

---


## Úloha 2.6

- riešenie v [stairs_4.c](stairs_4.c)

vo funkcií `climb_stair()` sme k prvej funkcii `step()` pridali `while` cyklus, pomocou ktorého robot vyšplhá ľubovoľne
vysoké schody

```c
while (right_is_blocked()) {
    step();
}
```

avšak, nepozbiera nám všetky beepre.

---

- riešenie v [stairs_5.c](stairs_5.c)

vo funkcií `climb_stair()` sme `if` podmienku 

```c
if (beepers_present()) {
    pick_beeper();
}
```

nahradili cyklom `while`

```c
while (beepers_present()) {
    pick_beeper();
}
```

tým pádom, robot pozbiera všetky dostupné beepre (ak tam nejaké sú)
