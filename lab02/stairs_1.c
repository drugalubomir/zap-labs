#include <superkarel.h>

int main() {
    turn_on("stairs.kw");

    step();
    turn_left();
    step();
    turn_left();
    turn_left();
    turn_left();
    step();
    pick_beeper();
    turn_left();
    step();
    turn_left();
    turn_left();
    turn_left();
    step();
    pick_beeper();
    turn_left();
    step();
    turn_left();
    turn_left();
    turn_left();
    step();
    pick_beeper();
    turn_left();
    step();
    turn_left();
    turn_left();
    turn_left();
    step();
    put_beeper();
    put_beeper();
    put_beeper();

    turn_off();

    return 0;
}
